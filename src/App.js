import logo from './logo.svg';
import './App.css';
import { useSelector, useDispatch } from 'react-redux';
import { setUser } from './slicer/userSlicer';
import axios from 'axios';
import { useEffect } from 'react';

function App() {

  const user = useSelector((state) => state.user)
  const dispatch = useDispatch();

  useEffect(
    () => {
      getUserData()
    },
    []
  )

  const getUserData = async () => {
    const getData = await axios.get('https://mocki.io/v1/53fecbb6-c3f1-44d5-9abf-1df0877d5dd9');
    
    dispatch(
      setUser({
        name: getData.data.name,
        address: getData.data.address
      })
    )
  }
  
  return (
    <div className="App">
      <p>Nama: {user.name}</p>
      <p>Alamat: {user.address}</p>

      <button onClick={getUserData}>Get User</button>
    </div>
  );
}

export default App;
