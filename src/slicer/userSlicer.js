import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  name: "",
  address: ""
}

export const userSlicer = createSlice({
  name: 'user',
  initialState: initialState,
  reducers: {
    setUser: (state, action) => {
        // action.payload = data user yang akan kita inject
        state.name = action.payload.name
        state.address = action.payload.address
    }
  },
})

// Action creators are generated for each case reducer function
export const { setUser } = userSlicer.actions

export default userSlicer.reducer